import React from 'react';
import './App.css';
import Users from './screens/Users';

export default function App(props) {
  return (
    <div className="App">
      <Users {...props} />
    </div>
  );
}
