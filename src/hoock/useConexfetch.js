// Dependencies
import React from 'react';
import { useState, useEffect, useCallback, useMemo } from "react";

export default function (props) {
  // States
  const [responseData, setResponseData] = useState(null);
  const [conexfetch, setConexfetch] = useState(null);

  useEffect(() => {
    const controller1 = new AbortController();
    (async () => {
      if ( getValidate(conexfetch)) {
        const { ruta, data, type } = conexfetch;
        data.signal = controller1.signal
        //console.log({ruta, data, type })
        await fetch(ruta, data)
          .then(res => res.json())
          .then(res => {
            if (res.msg !== "Error en respuesta") {
              setResponseData({...res, type});
            }
          })
          .catch((error) => {
            console.log(error);
          });

        setConexfetch(null);
      }
    })();

    return function cleanup() {
      controller1.abort();
    };
  }, [conexfetch]);


  const getValidate = useCallback(conex => {
    const show = conex && conex.type && conex.ruta && conex.ruta.length > 0 && conex.type.length > 0 ? true : false
    return show
  }, []);

  const conexion_fetch = useCallback((ruta, data, type) => {
    setConexfetch({ ruta, data, type });
  }, []);

  const result = useMemo(() =>{
    return({
      responseData,
      conexion_fetch,
      setResponseData
    })
  }, [responseData])

  // Return
  return result;  
}
