// Dependencies
import { useState, useEffect } from "react";
import useConexfetch from "./useConexfetch";

export default function (props) {
  // States
  const { responseData, conexion_fetch, setResponseData } = useConexfetch();
  const [detailUsers, setDetailUsers] = useState({ name: '',location: ''});

  useEffect(() => {
      conexion_fetch(`https://api.github.com/users/${props.id}`,{}, "getDetailUsers");
  }, []);

  useEffect(() => {
    const { type } = responseData ? responseData : {};
    if (type === "getDetailUsers" && responseData?.name) {
        setDetailUsers(responseData);
    }
    setResponseData(null);
  }, [responseData]);

  // Return
  return {
     ...detailUsers,
  };
}
